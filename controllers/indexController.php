<?php
class indexController extends Controller
{
	public function __construct() {
		parent::__construct();
	}
	
	public function index()
	{
		//$this->view->setJs(array('jquery'));
		$this->view->assign('welcome', 'Welcome to Wizard');
		$this->view->render('index');
	}
}
?>