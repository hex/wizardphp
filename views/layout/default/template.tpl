<html>
	<head>
		<title>WIZARD FRAMEWORK</title>
		<link href="{$layoutParams.css_route}" rel="stylesheet" type="text/css" />
		{foreach item=js from=$layoutParams.js}
		<script  src="{$js}" type="text/javascript"></script>
		{/foreach}
	</head>
	<body>
		<div id="header">
			<h1>{$layoutParams.config.app_name}</h1>
		</div>

		<div id="content">	
			{include file=$content}
		</div>

		<div id="footer">
			{$layoutParams.config.app_company}
		</div>
	</body>
</html>